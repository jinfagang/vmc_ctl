#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``camera`` module of ``input`` package."""

from typing import TypeVar
from threading import Thread
# Numpy
from numpy import (
    zeros,
    uint8,
    ndarray
)
# OpenCV
from cv2 import (  # pylint: disable=no-name-in-module
    VideoCapture,
    CAP_ANY
)
# VMCPS
from .typing import InputType
from alfred import logger

CameraT = TypeVar("CameraT", bound="Camera")


class Camera(Thread, InputType):
    """Camera input thread."""

    @property
    def is_open(self: CameraT) -> bool:
        """bool: Returns whether it's currently open."""
        return self._opened

    @property
    def end_of_data(self: CameraT) -> bool:
        """bool: Returns whether the input data reached its end."""
        return not self._not_end_of_stream

    def __init__(
        self: CameraT,
        device: int | str
    ) -> None:
        """Initialize input.

        Args:
            device (int):
                Device number.

        """
        super().__init__(
            group=None,
            target=None, args=(), kwargs={},
            name=f"camera{device}",
            daemon=False
        )
        self._opened = False
        self._not_end_of_stream = False
        self._device = device
        self._data = zeros((1, 1, 3), uint8)  # Image: 1 black RGB pixel
        self._stream = VideoCapture()

    def open(self: CameraT) -> CameraT:
        """Open camera.

        Returns:
            CameraT:
                Instance.

        Raises:
            IOError:
                If failed to open camera.

        """
        if not self.is_open:
            logger.info(f'open on video/camera: {self._device}')
            # if not self._stream.open(self._device, CAP_ANY):
            if not self._stream.open(self._device):
                raise IOError(f"Failed to open camera {self._device}.")
            self._opened = True
            self._not_end_of_stream = True
            self.start()
        return self

    def run(self: CameraT) -> None:
        """Grab camera frames."""
        while self._not_end_of_stream and self.is_open:
            self._not_end_of_stream, self._data = self._stream.read()
        self._stream.release()

    def read(self: CameraT) -> ndarray:
        """Return the latest frame.

        Returns:
            ndarray:
                Image data (BGR pixel pattern).

        Raises:
            IOError:
                If camera is not opened.

        """
        if self.is_open:
            return self._data
        else:
            raise IOError(f"Camera {self._device} not opened.")

    def close(self: CameraT) -> None:
        """Close camera."""
        if self.is_open:
            self._not_end_of_stream = False
            self._opened = False
            self.join()
