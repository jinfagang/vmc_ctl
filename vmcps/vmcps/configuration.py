#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``configuration`` module of ``vmcps`` package."""

from typing import Any
from os.path import exists
from yaml import safe_load, safe_dump, YAMLError  # type: ignore


class Configuration(dict):
    """Configuration."""

    filename: str

    def __setitem__(
        self,
        key: str,
        value: Any
    ) -> None:
        """Assign value to configuration key.

        Args:
            key (str):
                Configuration key.
            value (Any):
                Given alue.

        """
        super().__setitem__(key, value)
        with open(self.filename, 'w', encoding="utf-8") as file:
            safe_dump(dict(self), file)

    def __init__(
        self,
        filename: str,
        defaults: dict | None = None
    ) -> None:
        """Instantiate configuration.

        Args:
            filename (str):
                File path.
            defaults (dict | None):
                Default configuration (Optional).

        Raises:
            ValueError:
                If invalid configuration file (YAML format) is given.

        """
        self.filename = filename
        if defaults is None:
            defaults = {}
        if exists(self.filename):
            with open(self.filename, 'r', encoding="utf-8") as file:
                try:
                    defaults.update(safe_load(file))
                except YAMLError:
                    raise ValueError(
                        "Invalid configuration file given."
                    ) from YAMLError
        super().__init__(defaults)
